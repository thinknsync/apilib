package com.thinknsync.apilib;

public interface ErrorWrapper {
    void printError();
    Throwable getCause();
}
