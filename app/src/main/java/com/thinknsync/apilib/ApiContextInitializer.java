package com.thinknsync.apilib;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class ApiContextInitializer implements QueueContainer, AuthTokenContainer {

    private final String TAG = "API LIB";

    private RequestQueue mRequestQueue;
    private Context context;

    private static QueueContainer mInstance;
    private static String authToken;

    private ApiContextInitializer(Context context){
        this.context = context;
    }

    public static void init(Context context){
        if(mInstance == null){
            mInstance = new ApiContextInitializer(context);
        }
    }

    @Override
    public void setAuthToken(String token){
        authToken = token;
    }

    @Override
    public String getAuthToken(){
        return authToken;
    }

    ////////////////////////////////
    // volly functions
    public static synchronized QueueContainer getInstance() {
        return mInstance;
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(context);
        }

        return mRequestQueue;
    }

    @Override
    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    @Override
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
