package com.thinknsync.apilib;

public interface AuthTokenContainer {
    void setAuthToken(String token);
    String getAuthToken();
}
