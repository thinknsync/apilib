package com.thinknsync.apilib;

public interface ApiResponseView {
    void onApiCallSuccess(String... successMessage);
    void onApiCallFailure(String... failureMessage);
    void onApiCallError(String... errorMessage);
}
