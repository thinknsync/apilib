package com.thinknsync.apilib;

import android.app.ProgressDialog;

/**
 * Created by shuaib on 4/10/16.
 */
public interface ApiRequest {

    class ApiFields {
        public static final String KEY_MESSAGE = "message";
        public static final String KEY_STATUS = "success";
        public static final String KEY_RESULT = "result";
        public static final String KEY_DATA = "data";

        public static final String KEY_AUTH_TOKEN = "token";

        public static final String REQ_ID = "reqId";
        public static final String TAG = "reqId";
    }

    void setRetryCount(int retryCount);
    void setTimeout(int timeout);
    void sendRequest();
    void sendJsonRequest();
    void sendStringRequest();
    void setProgressDialog(ProgressDialog dialog);
    void setProgressCancellable(boolean cancellable);
    void setShouldShowAlertIfNoInternet(boolean showAlert);
}

