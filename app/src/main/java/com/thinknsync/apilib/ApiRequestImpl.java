package com.thinknsync.apilib;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ApplicationInfo;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.thinknsync.logger.Logger;
import com.thinknsync.networkutils.NetworkStatusReporter;
import com.thinknsync.networkutils.NetworkUtil;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.logger.AndroidLogger;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by shuaib on 4/7/16.
 */
public class ApiRequestImpl implements ApiRequest {

    private Context context;
    protected String url;
    protected ApiDataObject dataObject;
    private int requestMethod;
    private int timeout = (int) TimeUnit.SECONDS.toMillis(20);
    private int retryCount = 0;
    protected boolean shouldShowAlert = true;

    protected int reqId;
    protected ApiResponseActions volleyListenerAction;
    protected ProgressDialog progressDialog;

    protected Logger logger;
    private NetworkStatusReporter networkUtil;

    public ApiRequestImpl(final AndroidContextWrapper c, String url, ApiDataObject dataObject, int requestMethod,
                          ApiResponseActions volleyListenerAction, int reqid) {
        this.context = c.getFrameworkObject();
        this.url = url;
        this.dataObject = dataObject;
        this.requestMethod = requestMethod;
        this.volleyListenerAction = volleyListenerAction;
        this.reqId = reqid;
        try {
            this.progressDialog = new ProgressDialog(c.getFrameworkObject());
            progressDialog.setMessage("Please wait...");
        } catch (RuntimeException e){
            e.printStackTrace();
        }

        this.logger = AndroidLogger.getInstance(isDebuggable());
        this.networkUtil = new NetworkUtil(c);
    }

    private boolean isDebuggable(){
        return (0 != ((context).getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE));
    }

    @Override
    public void setShouldShowAlertIfNoInternet(boolean shouldShowAlert) {
        this.shouldShowAlert = shouldShowAlert;
    }

    @Override
    public void setProgressDialog(ProgressDialog dialog) {
        this.progressDialog = dialog;
    }

    @Override
    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    @Override
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    @Override
    public void setProgressCancellable(boolean cancellable){
        if(progressDialog != null){
            progressDialog.setCancelable(cancellable);
        }
    }

    @Override
    public void sendRequest(){
        dataObject.validateObject(dataObject.getObject());
//        addKeyValuePair(dataObject);

        if(dataObject.getObjectType().equals(dataObject.jsonType)){
            sendJsonRequest();
        } else if(dataObject.getObjectType().equals(dataObject.mapType)){
            sendStringRequest();
        } else if(dataObject.getObjectType().equals(dataObject.nullType)){
            sendRequestDefaultMethod();
        }
    }

    @Override
    public void sendJsonRequest(){
        if (networkUtil.isConnected(shouldShowAlert)) {
            if(progressDialog != null) {
                progressDialog.show();
            }

            sendJsonRequest(getRequestObject());
        } else {
            getResponseActionForNoInternet();
        }
    }

    private void getResponseActionForNoInternet() {
        volleyListenerAction.onApiCallError(reqId,  new VolleyErrorWrapper(new VolleyError("No Internet Connection")));
    }

    private void sendJsonRequest(final JSONObject reqObject) {
        JsonObjectRequest jsonRequest = new JsonObjectRequest(requestMethod, url, reqObject,
                getJsonRequestSuccessResponseListener(), getVolleyErrorResponseListener()){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                if (dataObject.getHeader() == null) {
                    return super.getHeaders();
                }
                return dataObject.getHeader();
            }

            @Override
            public byte[] getBody() {
                if (dataObject.getBody() == null) {
                    return super.getBody();
                }
                return dataObject.getBody().getBytes();
            }
        };
        jsonRequest.setRetryPolicy(getRequestPolicy());
        logger.debugLog("api data: ", reqObject.toString());
        ApiContextInitializer.getInstance().addToRequestQueue(jsonRequest);
    }

    @Override
    public void sendStringRequest() {
        if (networkUtil.isConnected(shouldShowAlert)) {
            if(progressDialog != null) {
                progressDialog.show();
            }

            sendJsonRequest(getRequestObject());
        } else {
            getResponseActionForNoInternet();
        }
    }

    protected JSONObject getRequestObject(){
        try {
            String authToken = ((AuthTokenContainer)ApiContextInitializer.getInstance()).getAuthToken();
            if(authToken != null){
                dataObject.addKeyValuePair(ApiFields.KEY_AUTH_TOKEN, authToken);
            }
            Object paramDataObject = dataObject.getObject();
            if(dataObject.getObjectType().equals(ApiDataObject.mapType)){
                paramDataObject = new JSONObject((Map)dataObject.getObject());
            }
            return new JSONObject().put("params", paramDataObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return new JSONObject((Map)dataObject.getObject());
    }

    protected void sendRequestDefaultMethod() {
        dataObject.setObj(new HashMap<>());
        sendStringRequest();
    }

    protected RetryPolicy getRequestPolicy() {
        return new DefaultRetryPolicy(timeout, retryCount, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    }

    protected Response.Listener<JSONObject> getJsonRequestSuccessResponseListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(progressDialog != null) {
                    progressDialog.dismiss();
                }

                callResponseAction(response);
            }
        };
    }

    protected Response.Listener<String> getStringRequestSuccessResponseListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(progressDialog != null) {
                    progressDialog.dismiss();
                }
                JSONObject responseJson = new JSONObject();
                try {
                    responseJson = new JSONObject(response);
                } catch (JSONException e) {
                    try {
                        responseJson = new JSONObject().put(ApiFields.KEY_STATUS, true)
                                .put(ApiFields.KEY_MESSAGE, "")
                                .put(ApiFields.KEY_DATA, response);
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                }
                callResponseAction(responseJson);
            }
        };
    }

    protected Response.ErrorListener getVolleyErrorResponseListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(progressDialog != null) {
                    progressDialog.dismiss();
                }
                /*if(error.networkResponse.data!=null) {
                    try {
                        String body = new String(error.networkResponse.data,"UTF-8");
                        System.out.println(body);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }*/
                logger.debugLog("response url ", url);
                volleyListenerAction.onApiCallError(reqId, new VolleyErrorWrapper(error));
            }
        };
    }

    protected void callResponseAction(JSONObject response){
        logger.debugLog("response url ", url);
        logger.debugLog("response ", response.toString());
        try {
            if(response.has(ApiFields.KEY_RESULT)) {
                if (response.getJSONObject(ApiFields.KEY_RESULT).getBoolean(ApiFields.KEY_STATUS)) {
                    volleyListenerAction.onApiCallSuccess(reqId, response.getJSONObject(ApiFields.KEY_RESULT));
                } else {
                    volleyListenerAction.onApiCallFailure(reqId, response.getJSONObject(ApiFields.KEY_RESULT));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
