package com.thinknsync.apilib;

import com.thinknsync.objectwrappers.ObjectValidator;

import java.util.Map;

/**
 * Created by shuaib on 10/16/16.
 */

public interface ApiDataObject extends ObjectValidator {
    String tag = "apiDataObject";
    String jsonType = "jsonType";
    String mapType = "mapType";
    String undefinedType = "undefined";
    String nullType = "null";

    Object getObject();
    void setObj(Object obj);
    Map getHeader();
    String getBody();
    void setHeader(Map header);
    void setBody(String body);
    String getObjectType();
    ApiDataObject addKeyValuePair(String key, String value);
    ApiDataObject addKeyValuePair(String key, int value);
    ApiDataObject addKeyValuePair(String key, boolean value);
    void removeKeyValuePair(String key);
}
