package com.thinknsync.apilib;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by shuaib on 4/10/16.
 */
public interface ApiResponseActions {
    String tag = "apiResponseActions";
    void onApiCallSuccess(int reqId, JSONObject responseJson) throws JSONException;
    void onApiCallFailure(int reqId, JSONObject responseJson) throws JSONException;
    void onApiCallError(int reqId, ErrorWrapper error);
}
