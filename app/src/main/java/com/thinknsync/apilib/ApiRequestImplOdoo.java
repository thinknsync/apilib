package com.thinknsync.apilib;

import com.android.volley.VolleyError;
import com.thinknsync.objectwrappers.AndroidContextWrapper;

import org.json.JSONException;
import org.json.JSONObject;

public class ApiRequestImplOdoo extends ApiRequestImpl {


    public ApiRequestImplOdoo(AndroidContextWrapper c, String url, ApiDataObject dataObject, int requestMethod, ApiResponseActions volleyListenerAction, int reqid) {
        super(c, url, dataObject, requestMethod, volleyListenerAction, reqid);
    }

    protected void callResponseAction(JSONObject response){
        try {
            String KEY_ERROR = "error";
            String KEY_NAME = "name";

            if(response.has(KEY_ERROR)){
                JSONObject errorData = response.getJSONObject(KEY_ERROR).getJSONObject(ApiFields.KEY_DATA);
                if(errorData.getString(KEY_NAME).startsWith("odoo.exceptions.")) {
                    volleyListenerAction.onApiCallError(reqId, new VolleyErrorWrapper(new VolleyError(errorData.toString())));
                }
            } else {
                super.callResponseAction(response);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
