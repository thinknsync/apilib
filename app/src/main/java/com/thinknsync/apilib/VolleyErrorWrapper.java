package com.thinknsync.apilib;

import com.android.volley.VolleyError;
import com.thinknsync.objectwrappers.BaseFrameworkWrapper;

public class VolleyErrorWrapper extends BaseFrameworkWrapper<VolleyError> implements ErrorWrapper {

    public static final String tag = "volleyError";

    public VolleyErrorWrapper(VolleyError volleyError) {
        setFrameworkObject(volleyError);
    }

    @Override
    public void printError() {
        getFrameworkObject().printStackTrace();
    }

    @Override
    public Throwable getCause() {
        return getFrameworkObject().getCause();
    }
}
