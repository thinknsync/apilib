package com.thinknsync.apilib;

import com.android.volley.VolleyError;
import com.thinknsync.objectwrappers.BaseFrameworkWrapper;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class TokenValidationResponseAction extends BaseResponseActions {

    private Runnable tokenExpirationRunnable;

    public TokenValidationResponseAction(Runnable expirationRunnable){
        this.tokenExpirationRunnable = expirationRunnable;
    }

    @Override
    public void onApiCallError(int reqId, ErrorWrapper error) {
        super.onApiCallError(reqId, error);
        try {
            String errorMessage = ((BaseFrameworkWrapper<VolleyError>)error).getFrameworkObject().getMessage();
            JSONObject errorJson = new JSONObject(errorMessage);
            if(errorJson.getString("name").contains("AccessDenied")){
                tokenExpirationRunnable.run();
            }
        } catch (JSONException | NullPointerException e){
            e.printStackTrace();
        }
    }
}
