package com.thinknsync.apilib;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class BaseResponseActions implements ApiResponseActions {

    @Override
    public void onApiCallFailure(int reqId, JSONObject responseJson) throws JSONException {

    }

    @Override
    public void onApiCallError(int reqId, ErrorWrapper error) {
        error.printError();
    }
}
